const User = require("../models/userModel");
const catchAsync = require("../utils/catchAsync");

const createUser = catchAsync(async (req, res) => {
  const user = new User(req.body);

  await user.save();
  const token = await user.generateAuthToken();
  res.status(201).json({
    status: "success",
    data: {
      user,
      token,
    },
  });
});

const loginUser = catchAsync(async (req, res) => {
  const user = await User.findByCredentials(req.body.email, req.body.password);
  const token = await user.generateAuthToken();
  res.status(200).json({
    status: "success",
    data: {
      user,
      token,
    },
  });
});

const logoutUser = catchAsync(async (req, res) => {
  req.user.tokens = req.user.tokens.filter((token) => {
    return token.token !== req.token;
  });
  await req.user.save();

  res.status(204).json({
    status: "success",
    data: null,
  });
});

const logoutAllUser = catchAsync(async (req, res) => {
  req.user.tokens = [];
  await req.user.save();
  res.status(204).json({
    status: "success",
    data: null,
  });
});

module.exports = {
  createUser,
  loginUser,
  logoutUser,
  logoutAllUser,
};
