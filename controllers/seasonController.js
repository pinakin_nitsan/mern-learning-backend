const Season = require("../models/seasonModel");

const getAllSeasons = async (req, res) => {
  try {
    // const seasons = await Season.find().select("-places");
    const seasons = await Season.find();

    res.status(200).json({
      status: "success",
      results: seasons.length,
      data: {
        seasons,
      },
    });
  } catch (err) {
    res.status(400).json({
      status: "fail",
      message: err,
    });
  }
};

const getSeason = async (req, res) => {
  try {
    // const season = await Season.findById(req.params.id);
    const season = await Season.findOne({ slug: req.params.id }).populate(
      "places",
      "name thumbnail urlSlug slug"
    );

    if (!season) {
      return next(new AppError("No season found with that ID", 404));
    }

    res.status(200).json({
      status: "success",
      data: {
        season,
      },
    });
  } catch (err) {
    res.status(400).json({
      status: "fail",
      message: err,
    });
  }
};

const createSeason = async (req, res) => {
  try {
    const newSeason = new Season({
      ...req.body,
      slug: `${slug(req.body.name)}`,
      urlSlug: `/${slug(req.body.name)}`,
    });
    await newSeason.save();
    res.status(201).json({
      status: "success",
      data: {
        season: newSeason,
      },
    });
  } catch (err) {
    res.status(400).json({
      status: "fail",
      message: err,
    });
  }
};

const updateSeason = async (req, res) => {
  const updates = Object.keys(req.body);
  const allowedUpdates = ["title", "name", "thumbnail", "places"];
  const isValidOperation = updates.every((update) =>
    allowedUpdates.includes(update)
  );

  if (!isValidOperation) {
    return res
      .status(400)
      .json({ status: "fail", message: "Invalid updates!" });
  }

  try {
    const season = await Season.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
      runValidators: true,
    });

    if (!season) {
      return next(new AppError("No season found with that ID", 404));
    }

    res.status(200).json({
      status: "success",
      data: {
        season,
      },
    });
  } catch (err) {
    res.status(400).json({
      status: "fail",
      message: err,
    });
  }
};

const deleteSeason = async (req, res) => {
  try {
    const season = await Season.findByIdAndDelete(req.params.id);

    if (!season) {
      return next(new AppError("No season found with that ID", 404));
    }

    res.status(204).json({
      status: "success",
      data: null,
    });
  } catch (err) {
    res.status(404).json({
      status: "fail",
      message: err,
    });
  }
};

module.exports = {
  getAllSeasons,
  getSeason,
  createSeason,
  updateSeason,
  deleteSeason,
};
