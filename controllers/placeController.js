const slug = require("slug");
const catchAsync = require("../utils/catchAsync");
const AppError = require("./../utils/appError");
const Place = require("../models/placeModel");
const Season = require("../models/seasonModel");
const Category = require("../models/categoryModel");

const getAllPlaces = catchAsync(async (req, res) => {
  let query;

  query = Place.find();

  const places = await query;

  res.status(200).json({
    status: "success",
    results: places.length,
    data: {
      places,
    },
  });
});

const getPlace = catchAsync(async (req, res) => {
  const place = await Place.findById(req.params.id)
    .populate("categories", "name slug urlSlug")
    .populate("seasons", "name slug urlSlug");

  if (!place) {
    return next(new AppError("No place found with that ID", 404));
  }

  res.status(200).json({
    status: "success",
    data: {
      place,
    },
  });
});

const createPlace = catchAsync(async (req, res) => {
  const newPlace = await Place.create({
    ...req.body,
    slug: `${slug(req.body.title)}`,
    urlSlug: `/places/${slug(req.body.title)}`,
  });

  await Season.updateMany(
    { _id: newPlace.seasons },
    { $push: { places: newPlace._id } }
  );

  await Category.updateMany(
    { _id: newPlace.categories },
    { $push: { places: newPlace._id } }
  );

  res.status(201).json({
    status: "success",
    data: {
      place: newPlace,
    },
  });
});

const updatePlace = catchAsync(async (req, res) => {
  const updates = Object.keys(req.body);
  const allowedUpdates = [
    "title",
    "thumbnail",
    "images",
    "categories",
    "seasons",
  ];
  const isValidOperation = updates.every((update) =>
    allowedUpdates.includes(update)
  );

  if (!isValidOperation) {
    return res
      .status(400)
      .json({ status: "fail", message: "Invalid updates!" });
  }

  const place = await Place.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true,
  });

  if (!place) {
    return next(new AppError("No place found with that ID", 404));
  }

  res.status(200).json({
    status: "success",
    data: {
      place,
    },
  });
});

const deletePlace = catchAsync(async (req, res) => {
  const place = await Place.findByIdAndDelete(req.params.id);

  if (!place) {
    return next(new AppError("No place found with that ID", 404));
  }

  res.status(204).json({
    status: "success",
    data: null,
  });
});

module.exports = {
  getAllPlaces,
  getPlace,
  createPlace,
  updatePlace,
  deletePlace,
};
