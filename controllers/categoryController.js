const slug = require("slug");
const Category = require("../models/categoryModel");
const catchAsync = require("../utils/catchAsync");
const AppError = require("../utils/AppError");

const getAllCategories = catchAsync(async (req, res) => {
  const queryObj = { ...req.query };
  const { type } = req.query;
  let query;

  if (type) {
    console.log(type, "type");
    query = Category.find({ [type]: true }).select(
      "-places -popular -city -season"
    );
  } else {
    query = Category.find().select("-places -popular -city -season");
  }

  const categories = await query;
  res.status(200).json({
    status: "success",
    results: categories.length,
    data: {
      categories,
    },
  });
});

const getCategory = catchAsync(async (req, res, next) => {
  const category = await Category.findOne({ slug: req.params.id }).populate(
    "places",
    "title thumbnail urlSlug slug"
  );

  if (!category) {
    return next(new AppError("No category found with that ID", 404));
  }

  res.status(200).json({
    status: "success",
    data: {
      category,
    },
  });
});

const createCategory = catchAsync(async (req, res) => {
  const newCategory = await Category.create({
    ...req.body,
    slug: `${slug(req.body.name)}`,
    urlSlug: `/${slug(req.body.name)}`,
  });
  res.status(201).json({
    status: "success",
    data: {
      category: newCategory,
    },
  });
});

const updateCategory = catchAsync(async (req, res) => {
  const updates = Object.keys(req.body);
  const allowedUpdates = [
    "title",
    "name",
    "thumbnail",
    "popular",
    "city",
    "season",
  ];
  const isValidOperation = updates.every((update) =>
    allowedUpdates.includes(update)
  );

  if (!isValidOperation) {
    return res
      .status(400)
      .json({ status: "fail", message: "Invalid updates!" });
  }

  let queryObj = null;

  if (req.body.name) {
    queryObj = {
      ...req.body,
      slug: `${slug(req.body.name)}`,
      urlSlug: `/${slug(req.body.name)}`,
    };
  } else {
    queryObj = {
      ...req.body,
    };
  }

  const category = await Category.findByIdAndUpdate(
    req.params.id,
    {
      ...queryObj,
    },
    {
      new: true,
      runValidators: true,
    }
  );

  if (!category) {
    return next(new AppError("No category found with that ID", 404));
  }

  res.status(200).json({
    status: "success",
    data: {
      category,
    },
  });
});

const deleteCategory = catchAsync(async (req, res) => {
  const category = await Category.findByIdAndDelete(req.params.id);

  if (!category) {
    return next(new AppError("No category found with that ID", 404));
  }

  res.status(204).json({
    status: "success",
    data: null,
  });
});

module.exports = {
  getAllCategories,
  getCategory,
  createCategory,
  updateCategory,
  deleteCategory,
};
