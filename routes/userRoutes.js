const express = require("express");
const userController = require("../controllers/userController");
const auth = require("../middleware/auth");

const router = express.Router();

router.route("/").post(userController.createUser);
router.route("/login").post(userController.loginUser);
router.route("/logout").post(auth, userController.logoutUser);
router.route("/logoutAll").post(auth, userController.logoutAllUser);

module.exports = router;
