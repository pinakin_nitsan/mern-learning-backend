const express = require("express");
const auth = require("../middleware/auth");
const seasonController = require("../controllers/seasonController");

const router = express.Router();

router
  .route("/")
  .get(seasonController.getAllSeasons)
  .post(auth, seasonController.createSeason);

router
  .route("/:id")
  .patch(auth, seasonController.updateSeason)
  .delete(auth, seasonController.deleteSeason)
  .get(seasonController.getSeason);

module.exports = router;
