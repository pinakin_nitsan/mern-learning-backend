const express = require("express");
const auth = require("../middleware/auth");
const categoryController = require("../controllers/categoryController");

const router = express.Router();

router
  .route("/")
  .get(categoryController.getAllCategories)
  .post(auth, categoryController.createCategory);

router
  .route("/:id")
  .patch(auth, categoryController.updateCategory)
  .delete(auth, categoryController.deleteCategory)
  .get(categoryController.getCategory);

module.exports = router;
