const express = require("express");
const auth = require("../middleware/auth");
const placeController = require("../controllers/placeController");

const router = express.Router();

// router
//   .route("/top-5-cheap")
//   .get(placeController.aliasTopPlaces, placeController.getAllPlaces);

router
  .route("/")
  .get(placeController.getAllPlaces)
  .post(auth, placeController.createPlace);

router
  .route("/:id")
  .get(placeController.getPlace)
  .patch(auth, placeController.updatePlace)
  .delete(auth, placeController.deletePlace);

module.exports = router;
