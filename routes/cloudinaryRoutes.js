const express = require("express");
const auth = require("../middleware/auth");
const cloudinaryController = require("../controllers/cloudinaryController");

const router = express.Router();

router.route("/uploadimages").post(auth, cloudinaryController.uploadImage);
router.route("/removeimage").delete(auth, cloudinaryController.removeImage);

module.exports = router;
