const express = require("express");
const cors = require("cors");
const morgan = require("morgan");

const AppError = require("./utils/appError");
const globalErrorHandler = require("./controllers/errorController");
const userRouter = require("./routes/userRoutes");
const placeRouter = require("./routes/placeRoutes");
const seasonRouter = require("./routes/seasonRoutes");
const categoryRouter = require("./routes/categoryRoutes");
const cloudinaryRouter = require("./routes/cloudinaryRoutes");

const app = express();

app.use(cors());

if (process.env.NODE_ENV === "development") {
  app.use(morgan("dev"));
}
app.use(express.json({ limit: "50mb" }));
app.use(express.urlencoded({ limit: "50mb" }));
app.use(express.static(`${__dirname}/public`));

app.use((req, res, next) => {
  req.requestedAt = new Date().toISOString();
  next();
});

app.use("/api/users", userRouter);
app.use("/api/seasons", seasonRouter);
app.use("/api/places", placeRouter);
app.use("/api/categories", categoryRouter);
app.use("/api", cloudinaryRouter);

app.all("*", (req, res, next) => {
  next(new AppError(`Can't find ${req.originalUrl} on this server!`, 404));
});

app.use(globalErrorHandler);

module.exports = app;
