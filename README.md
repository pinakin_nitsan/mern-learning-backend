# MERN Learning Backend

## Available Scripts

In the project directory, you can run:

### `yarn dev`

Runs the app in the development mode.\
Open [http://localhost:3300/api](http://localhost:3300/api) to view it in the browser.
