const mongoose = require("mongoose");

const categorySchema = new mongoose.Schema({
  title: {
    type: String,
    required: [true, "A season must have a title"],
    unique: true,
  },
  name: {
    type: String,
    required: [true, "A category must have a name"],
    unique: true,
  },
  thumbnail: {
    type: String,
    required: [true, "A category must have a thumbnail"],
  },
  popular: {
    type: Boolean,
    default: false,
    required: false,
  },
  season: {
    type: Boolean,
    default: false,
    required: false,
  },
  city: {
    type: Boolean,
    default: false,
    required: false,
  },
  places: [{ type: mongoose.Types.ObjectId, ref: "Place" }],
  slug: String,
  urlSlug: String,
});

const Category = mongoose.model("Category", categorySchema);

module.exports = Category;
