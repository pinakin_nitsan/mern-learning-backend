const mongoose = require("mongoose");

const seasonSchema = new mongoose.Schema({
  title: {
    type: String,
    required: [true, "A season must have a title"],
    unique: true,
  },
  name: {
    type: String,
    required: [true, "A season must have a name"],
    unique: true,
  },
  thumbnail: {
    type: String,
    required: [true, "A season must have a thumbnail"],
  },
  places: [{ type: mongoose.Types.ObjectId, ref: "Place" }],
  slug: String,
  urlSlug: String,
});

const Season = mongoose.model("Season", seasonSchema);

module.exports = Season;
