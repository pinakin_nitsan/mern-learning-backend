const mongoose = require("mongoose");

const placeSchema = new mongoose.Schema({
  title: {
    type: String,
    required: [true, "A place must have a title"],
    unique: true,
  },
  thumbnail: {
    type: String,
    required: [true, "A place must have a thumbnail"],
  },
  images: {
    type: [String],
    required: [true, "A place must have atleast one image"],
  },
  slug: String,
  urlSlug: String,
  categories: [
    {
      // type: mongoose.Schema.Types.ObjectId,
      type: mongoose.Types.ObjectId,
      ref: "Category",
    },
  ],
  seasons: [
    {
      // type: mongoose.Schema.Types.ObjectId,
      type: mongoose.Types.ObjectId,
      ref: "Season",
    },
  ],
  author: {
    type: String,
  },
});

const Place = mongoose.model("Place", placeSchema);

module.exports = Place;
